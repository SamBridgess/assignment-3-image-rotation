#ifndef image_functions
#define image_functions
#include <stdlib.h>
#include <stdint.h>

struct __attribute__((packed))pixel {
    uint8_t r, g, b;
};

struct image {
    uint64_t width, height;
    struct pixel* pixels_array;
};

struct image create_new_image(uint64_t width, uint64_t height);

struct image rotate_image(const struct image* source);

struct pixel get_pixel(const struct image* img, uint64_t x, uint64_t y);

void set_pixel(struct image* img, uint64_t x, uint64_t y, struct pixel pixel);
#endif

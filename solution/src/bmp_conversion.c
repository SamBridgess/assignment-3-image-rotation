#include "bmp_conversion.h"
#include "image_functions.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define CORRECT_SIGNATURE 19778
#define RESERVED 0
#define DATA_OFFSET 54
#define HEADER_SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define PPM 2834
#define COLORS_USED 0
#define COLORS_IMPORTANT 0

static long calculate_padding(uint64_t width) {
    long temp = (long)width*3;
    if(temp % 4 == 0) return 0;
    else return 4 - (temp % 4);
}
enum read_status bmp_to_stucture(FILE* in, struct image* img) {
 
    struct bmp_header header;
    if(!fread(&header, sizeof(struct bmp_header), 1, in)) return READ_INVALID_HEADER;
    if(header.signature != CORRECT_SIGNATURE) return READ_INVALID_SIGNATURE;  
    if(header.bit_count != 24) return READ_INVALID_BITS;
    
    *img = create_new_image(header.width, header.height);

    long padding = calculate_padding(img->width);
    for(uint64_t yy = img->height; yy > 0 ; yy--) {
        uint64_t y = yy - 1;
        if(!fread(img->pixels_array + y * img->width, sizeof(struct pixel), img->width, in)) return READ_INVALID_BITS;
        for (int x = 0; x < img->width; x++) 
            set_pixel(img, x, y, get_pixel(img, x, y));     
        if(fseek(in, padding, SEEK_CUR)) return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum write_status structure_to_bmp(FILE* out, const struct image* img) {
    long padding = calculate_padding(img->width);

    struct bmp_header header;
    header.signature = CORRECT_SIGNATURE;
    header.image_size = (img->width*3 + padding) * img->height;
    header.file_size = header.image_size + sizeof(struct bmp_header);
    header.reserved = RESERVED;
    header.data_offset = DATA_OFFSET;
    header.size = HEADER_SIZE;
    header.width = img->width;
    header.height = img->height;
    header.planes = PLANES;
    header.bit_count = BIT_COUNT;
    header.compression = COMPRESSION;
    header.x_pixels_per_m = PPM;
    header.y_pixels_per_m = PPM;
    header.colors_used = COLORS_USED;
    header.colors_important = COLORS_IMPORTANT;

    if(!fwrite(&header, sizeof(struct bmp_header), 1, out)) return WRITE_ERROR;
    uint64_t line_size = sizeof(uint8_t) * (img->width * 3 + padding);
    for (uint64_t yy = img->height; yy > 0 ; yy--) {
        uint64_t y = yy - 1;
        uint8_t* line = (uint8_t*) malloc(line_size);


        for (uint64_t x = 0; x < img->width; x++) {
            line[3*x] = get_pixel(img, x, y).r;
            line[3*x+1] = get_pixel(img, x, y).g;
            line[3*x+2] = get_pixel(img, x, y).b;
        }

        for (uint64_t x = 0; x < padding; x++)
            line[img->width*3 + x] = 0;
        

        if(!fwrite(line, sizeof(uint8_t), img->width*3+padding, out)) {
            free(line);
            return WRITE_ERROR;
        }
        free(line);
    }
    return WRITE_OK;
}

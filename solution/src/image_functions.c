#include "image_functions.h"
#include <stdint.h>

struct image create_new_image(uint64_t width, uint64_t height) {
    //struct image* img = (struct image*) malloc(sizeof(struct image));
    struct image img = {
        .pixels_array = (struct pixel*) malloc(sizeof(struct pixel) * width * height),
        .width = width,
        .height = height 
    };

    return img;
}

struct image rotate_image(const struct image* source) {
    //height and width are swaped
    struct image img = create_new_image(source->height, source->width);

    for (uint64_t y = 0; y < source->height; y++)
        for(uint64_t x = 0; x < source->width; x++) 
            set_pixel(&img, y, source->width - x - 1, get_pixel(source, x, y));
        
    return img;
}

struct pixel get_pixel(const struct image* img, uint64_t x, uint64_t y) {
    return img->pixels_array[y*img->width + x];
}

void set_pixel(struct image* img, uint64_t x, uint64_t y, const struct pixel pixel) {
    img->pixels_array[y*img->width + x] = pixel;
   // img->pixels_array[y*img->width + x].r = pixel.r;
    //img->pixels_array[y*img->width + x].g = pixel.g;
    //img->pixels_array[y*img->width + x].b = pixel.b;
}    


#include "bmp_conversion.h"
#include "image_functions.h"
#include <inttypes.h>
#include <stdio.h>

int main(int argc, char** argv) {
    if (argc != 3) {     
        printf("Invalid number of arguments!\n");
        return 1;
    }
    
    FILE* in = NULL;
    if(!(in = fopen(argv[1], "rb"))){
        printf("Unable to open this file\n");
        return 1;
    }
   

    struct image in_img = {0};
    switch(bmp_to_stucture(in, &in_img)) {
        case READ_INVALID_BITS:
            printf("Invalid bits error\n");
            return 1;
        case READ_INVALID_HEADER:
            printf("Invalid header error\n");
            return 1;
        case READ_INVALID_SIGNATURE:
            printf("Invalid signature error\n");
            return 1;
        case READ_OK:
            printf("Read success\n");   
    }
    
    struct image rotated_img = rotate_image(&in_img);


    FILE* out = NULL;
    if(!(out = fopen(argv[2], "wb"))) {
        printf("Unable to write to this destination\n");
        return 1;
    }
    switch (structure_to_bmp(out, &rotated_img)){
        case WRITE_ERROR:
            printf("Write error\n");
            return 1;
        case WRITE_OK:
            printf("Write success\n");
    }
    
    free(in_img.pixels_array);
    free(rotated_img.pixels_array);
    if(fclose(in) != 0) printf("Failed to close in");
    if(fclose(out) != 0) printf("Failed to close out");
    return 0;
}
